function ps() {
  document.querySelector('video').playbackRate = 1.25;
}

chrome.action.onClicked.addListener((tab) => {
  if(!tab.url.includes("chrome://")) {
    chrome.scripting.executeScript({
      target: { tabId: tab.id },
      function: ps
    });
  }
});
