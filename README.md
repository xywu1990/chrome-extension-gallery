# Chrome extension gallery

This is a collection of Chrome extensions that are usefull which includes:

1. One-click to enable fullscreen,
2. One-click to enable video playing speed to be set to {1.0, 1.5, 1.75, 2.0} times.

To use the Chrome extensions, simply download the files in each folder and zip them into one .zip file. Then, change the extension fo the file to .crx and you are ready to install it in Chrome.
